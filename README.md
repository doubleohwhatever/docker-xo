# docker-xo

Simple docker file to build an up-to-date Xen Orchestra container. Forked from https://github.com/jpoa/docker-xo and modified to:

1) Automatically update when the container is started
2) Expose /Storage as a directory for locally stored backups.

# Notes

1) You will need to remove the default local backup datastore and /Storage

2) Xen Orchestra uses Redis for its database. Data is stored in memory and is only written to disk every fifteen minutes or so.
