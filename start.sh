#!/bin/bash
cd /app/xo-server/
git pull --ff-only
npm install
npm run build
cd /app/xo-web/
git pull --ff-only
npm install
npm run build
/usr/bin/supervisord
